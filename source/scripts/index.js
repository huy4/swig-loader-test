function jsHint(input, options) {
    // copy options to own object
    if(this.options && this.options.jshint) {
        for(var name in this.options.jshint) {
            options[name] = this.options.jshint[name];
        }
    }
}
