const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SocialTags = require('social-tags-webpack-plugin');

module.exports = {
  entry: './source/scripts/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
    {
        test: /\.hbs$/,
        loader: 'handlebars-loader'
    },
    {
      test: /\.(gif|jpg|png|ico)(\?.*$|$)$/,
      loader: 'file-loader',
    },
]
  },
  plugins: [
    new HtmlWebpackPlugin({
        title: "Webpack 4 a",
        filename: "index.html",
        template: "source/templates/index.hbs",
        imageTest: "./test.jpg",
    }),
  ]
};
